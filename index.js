var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var stringSimiliraity = require("string-similarity");
var methodOverride = require("method-override");
var async = require("async");
var nodemailer = require("nodemailer");
var ical = require("ical-generator");
var cal = ical({ domain: "localhost:3000", name: "my first reminder" });

mongoose.connect(
  "mongodb://diegordz1:diegordz1@ds163694.mlab.com:63694/textcomparision"
);

app.use(bodyParser.urlencoded({ extended: true }));
app.set("view engine", "ejs");
app.use(methodOverride("_method"));

var TextSchema = new mongoose.Schema({
  skill: String
});

var MailSchema = new mongoose.Schema({
  mail: String
});

var Skill = mongoose.model("Text", TextSchema);
var Mail = mongoose.model("Mail", MailSchema);

let transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 587,
  secure: false,
  auth: {
    user: "diego.pcde@gmail.com",
    pass: "etuplkzmrugndpff"
  }
});

app.get("/", function(req, res) {
  Skill.find({}, function(err, allskills) {
    if (err) {
      console.log(err);
    } else {
      res.render("testinput", { skills: allskills });
      // res.send(allskills[1].skill);
    }
  });
});

app.get("/mailfunction", function(req, res) {
  Mail.find({}, function(err, allmails) {
    if (err) {
      console.log(err);
    } else {
      res.render("mailfunction", { mail: allmails });
      // res.send(allskills[1].skill);
    }
  });
});

app.post("/mailfunction", function(req, res) {
  cal.createEvent({
    start: new Date("2019-06-25T06:59:32.940Z"),
    end: new Date("2019-06-25T08:59:32.940Z"),
    summary: "Test",
    description: "it works",
    location: "PCDE",
    url: "http://localhost:3000",
    method: "request"
  });

  var path = "fadsfa.ics";
  cal.saveSync(path);
  // console.log(cal.toString());
  var mail = req.body.mail;
  if (mail != null) {
    var newMail = { mail: mail };
    Mail.create(newMail, function(err, newmailcreated) {
      if (err) {
        console.log(err);
      } else {
        res.redirect("/mailfunction");
      }
    });
  } else {
    var mailstosent = [];
    Mail.find({}, function(err, mailsfound) {
      for (var i = 0; i < mailsfound.length; i++) {
        mailstosent[i] = mailsfound[i].mail;
      }
      //   console.log(mailstosent);
    });
    var mailOptions = {
      from: "company2.pcde@gmail.com",
      to: mailstosent,
      subject: "tetSs",
      text: "this is a test messsage for all accounts2",
      icalEvent: {
        filename: "reminder.ics",
        method: "PUBLISH",
        content: cal.toString(),
        path: path
      }
    };
    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        res.redirect("/mailfunction");
        console.log("mail sent");
      }
    });
  }
});

app.post("/", function(req, res) {
  var skill = req.body.skill;
  // console.log(skill);
  if (skill != null) {
    var newText = { skill: skill };

    Skill.create(newText, function(err, newlyCreated) {
      if (err) {
        console.log(err);
      } else {
        //   console.log(newText)

        res.redirect("/");
      }
    });
  }
  //Compare words
  else {
    var skillcheck = req.body.skillcheck;
    var ratiocheck = Number(req.body.ratiocheck);

    Skill.find({}, function(err, allskills) {
      if (err) {
        console.log(err);
      } else {
        var result = "";
        // var matches = stringSimiliraity.findBestMatch(skillcheck, [allskills]);
        for (var i = 0; i < allskills.length; i++) {
          var skillsdb = allskills[i].skill;
          var similarity = stringSimiliraity.compareTwoStrings(
            skillcheck,
            skillsdb
          );

          if (similarity >= ratiocheck) {
            var result =
              result +
              "the similarity between " +
              skillcheck +
              " and " +
              skillsdb +
              " is " +
              similarity +
              " SHOULD CHANGE" +
              ratiocheck +
              "\n";
            Skill.update(allskills[i], skillcheck);
          } else {
            var result =
              result +
              "the similarity between " +
              skillcheck +
              " and " +
              skillsdb +
              " is " +
              similarity +
              " SHOULD NOT CHANGE" +
              ratiocheck +
              "\n";
          }

          // res.send("the similarity between " + skillcheck +  " and " + skillsdb + " is " + similarity)
          //   console.log(result)
        }
      }
      res.render("check", { result: result });
      // res.redirect("/");
    });
  }
});

app.delete("/", function(req, res) {
  Skill.remove({}, function(err) {
    if (err) {
      res.redirect("/");
    } else {
      res.redirect("/");
    }
  });
});

app.delete("/mailfunction", function(req, res) {
  Mail.remove({}, function(err) {
    if (err) {
      res.redirect("/mailfunction");
    } else {
      res.redirect("/mailfunction");
    }
  });
});

app.put("/", function(req, res) {
  var skillchange = req.body.skillchange;
  var ratiocheck = Number(req.body.ratiocheck);
  //Change the wrong word to the desire one
  Skill.find({}, function(err, allskills) {
    if (err) {
      console.log(err);
    } else {
      var result = "";
      // var matches = stringSimiliraity.findBestMatch(skillcheck, [allskills]);
      for (var i = 0; i < allskills.length; i++) {
        var skillsdb = allskills[i].skill;
        var similarity = stringSimiliraity.compareTwoStrings(
          skillchange,
          skillsdb
        );

        if (similarity >= ratiocheck) {
          // console.log(allskills[i].skill)
          Skill.update(
            { skill: allskills[i].skill },
            { skill: skillchange },
            function(err) {
              //   Skill.update({skill: 'update'}, {skill: skillchange}, function(err){
              //   Skill.update({skill: 'Test'}, {skill: skillchange}, function(err){
              if (err) {
              }
            }
          );
        }
      }
    }
    res.redirect("/");
    // res.redirect("/");
  });
});

app.get("/check", function(req, res) {
  Skill.find({}, function(err, allskills) {
    if (err) {
      console.log(err);
    } else {
      res.render("check", { skills: allskills });
      // res.send(allskills[1].skill);
    }
  });
});

// app.post("/check", function(req,res){
//     var skillcheck = req.body.skillcheck
//      Skill.find({}, function(err, allskills){
//             if(err){
//                 console.log(err);
//             } else {

//                 // var matches = stringSimiliraity.findBestMatch(skillcheck, [allskills]);
//                 for(var i = 0; i< allskills.length; i++){
//                 var skillsdb = allskills[i].skill;
//                 var similarity = stringSimiliraity.compareTwoStrings(skillcheck, skillsdb);
//                 console.log(similarity)
//                 }
//             }
//             res.redirect("/");

//         });

// })

// var similarity = stringSimiliraity.compareTwoStrings('te', 'test');
//                 console.log(similarity)

// console.log(matches);
// console.log(similarity)

// if(similarity >= .5){
//     console.log("the text is similar" + " with this amount of certatly " + similarity)
// } else {
//     console.log("the text is not similatr" + " with this amount of certatly " + similarity);
// }
// Use environment defined port or 3000
var port = process.env.PORT || 3000;

// Start the server
app.listen(port);
console.log("Server starts on port " + port);
